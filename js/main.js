$(document).ready(function () {

    var owl2 = document.getElementById('owl-carousel2')
    $('#owl2').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        rewind: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
                loop: false
            }
        }
    })
    $('#owl3').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        rewind: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    })
    $('#owl2 .owl-prev').html("<img src='img/icons/left.png' />")
    $('#owl2 .owl-next').html("<img src='img/icons/right.png' />")
    $('#owl3 .owl-prev').html("<img src='img/icons/wleft.png' />")
    $('#owl3 .owl-next').html("<img src='img/icons/wright.png' />")

    // Menu links hover effect
    $('.navigation2 .links li a').on('mouseover', function () {
        TweenMax.to($(this), .3, {color: "#027a3e"});
    }).on('mouseleave', function () {
        TweenMax.to($(this), .3, {color: "black"});
    })
    // Menu links hover effect

    var testimonial = true;
    $('.moreBtn').click(function () {
        if (testimonial) {
            TweenMax.to($('.testimonial2'), 0.2, {top: 0})
            $('.moreBtn').html("<img src='img/icons/down.png' /><span>SPUSTI</span>")
            testimonial = false;
        } else {
            TweenMax.to($('.testimonial2'), 0.2, {top: 285})
            $('.moreBtn').html("<img src='img/icons/up.png' /><span>PROČITAJ VIŠE</span>")
            testimonial = true;
        }
    })
    var activeMenu = true;
    $('.burgerParent').click(function () {
        if (activeMenu) {
            TweenMax.to($('.burger1'), 0.2, {rotation: 45, top: 5})
            TweenMax.to($('.burger2'), 0.2, {alpha: 0})
            TweenMax.to($('.burger3'), 0.2, {rotation: -45, top: 5})
            TweenMax.to($('.mobileMenu'), 0.5, {top: 0, alpha: 1})
            activeMenu = false;
        } else {
            TweenMax.to($('.burger1'), 0.2, {rotation: 0, top: -2})
            TweenMax.to($('.burger2'), 0.2, {alpha: 1})
            TweenMax.to($('.burger3'), 0.2, {rotation: 0, top: 12})
            TweenMax.to($('.mobileMenu'), 0.5, {top: "-100%", alpha: 0})
            activeMenu = true;
        }
    })
    $('.mobileNavigation ul li a').click(function () {
        TweenMax.to($('.burger1'), 0.2, {rotation: 0, top: 0});
        TweenMax.to($('.burger2'), 0.2, {alpha: 1});
        TweenMax.to($('.burger3'), 0.2, {rotation: 0, top: 20});
        TweenMax.to($('.mobileNavigation'), 0.5, {top: "100%", alpha: 0});
        activeMenu = true;
    })

    $(document).on('scroll', function () {
        if ($(document).scrollTop() > 150) {
            $('.backTo').css('display', 'block');
            TweenMax.to($('.backTo'), 1, {alpha: 1});
        } else {
            TweenMax.to($('.backTo'), 1, {alpha: 0});
            $('.backTo').css('display', 'none');
        }
    })
    // Smooth Scroll ///////////////////////
    $(function () {
        $('a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    // Smooth Scroll ///////////////////////

    $('.bottomBlogPreview li').on('mouseover', function () {
        var ind = $(this).index() + 1;
        $("#blogPreviewDiv" + ind).css('display', 'block');
    }).on('mouseleave', function () {
        var ind = $(this).index() + 1;
        $("#blogPreviewDiv" + ind).css('display', 'none');
    })

    TweenLite.to($('.slogan'), 2, {
        text: "Digitalni marketing, ki vam pripelje nove kupce in obstoječim proda več.",
        ease: Linear.easeNone
    });
    TweenLite.to($('.arrow'), 1, {
        alpha: 1,
        delay: 2
    })

    var mobileMenu = true;
    $('.burgerParent').on('click', function () {
        if (mobileMenu) {
            TweenMax.to($('.mobileMenu'), 0.5, {left: 0})
            $('body').css('overflow', 'hidden');
            mobileMenu = false;
        } else {
            TweenMax.to($('.mobileMenu'), 0.5, {left: '100%'})
            $('body').css('overflow-y', 'scroll');
            mobileMenu = true;
        }
    })
})

function owlSlide(index) {
    var slide = index - 1;
    $('#owl3').trigger('to.owl.carousel', [slide, 500, true]);
}
